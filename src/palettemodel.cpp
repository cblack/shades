// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <QDebug>
#include <QtMath>

#include "palettemodel.h"
#include "csscolorparser.h"

struct PaletteModel::Private
{
    QColor color;
    QColor colors[20];
};

PaletteModel::PaletteModel(QObject* parent) : QAbstractListModel(parent), d(new Private)
{

}

PaletteModel::~PaletteModel()
{

}

QColor PaletteModel::color() const
{
    return d->color;
}

const auto cl = [](qreal f) { return qBound(0.0, f, 1.0); };

struct RGB { qreal r = 0; qreal g = 0; qreal b = 0; };
struct LAB { qreal L = 0; qreal a = 0; qreal b = 0; };

LAB linearSRGBToOKLab(RGB c) {
  const auto l = 0.4122214708 * c.r + 0.5363325363 * c.g + 0.0514459929 * c.b;
  const auto m = 0.2119034982 * c.r + 0.6806995451 * c.g + 0.1073969566 * c.b;
  const auto s = 0.0883024619 * c.r + 0.2817188376 * c.g + 0.6299787005 * c.b;

  const auto l_ = qPow(l, 1.0/3.0);
  const auto m_ = qPow(m, 1.0/3.0);
  const auto s_ = qPow(s, 1.0/3.0);

  return LAB {
    .L = 0.2104542553 * l_ + 0.7936177850 * m_ - 0.0040720468 * s_,
    .a = 1.9779984951 * l_ - 2.4285922050 * m_ + 0.4505937099 * s_,
    .b = 0.0259040371 * l_ + 0.7827717662 * m_ - 0.8086757660 * s_
  };
}

RGB OKLabToLinearSRGB(LAB lab) {
  const auto l_ = lab.L + 0.3963377774 * lab.a + 0.2158037573 * lab.b;
  const auto m_ = lab.L - 0.1055613458 * lab.a - 0.0638541728 * lab.b;
  const auto s_ = lab.L - 0.0894841775 * lab.a - 1.2914855480 * lab.b;

  const auto l = l_ * l_ * l_;
  const auto m = m_ * m_ * m_;
  const auto s = s_ * s_ * s_;

  return RGB {
    .r = +4.0767416621 * l - 3.3077115913 * m + 0.2309699292 * s,
    .g = -1.2684380046 * l + 2.6097574011 * m - 0.3413193965 * s,
    .b = -0.0041960863 * l - 0.7034186147 * m + 1.7076147010 * s
  };
}

RGB linearSRGBToSRGB(RGB rgb)
{
    const auto f = [](qreal x) {
        if (x >= 0.0031308) return 1.055 * (qPow(x, (1.0 / 2.4))) - 0.055;
        else return 12.92 * x;
    };
    return RGB {
        .r = f(rgb.r),
        .g = f(rgb.g),
        .b = f(rgb.b)
    };
}

RGB SRGBToLinearSRGB(RGB rgb) {
  const auto f = [](qreal x) {
    if (x >= 0.04045) return qPow(((x + 0.055) / (1 + 0.055)), 2.4);
    else return x / 12.92;
  };
  return RGB {
    .r = f(rgb.r),
    .g = f(rgb.g),
    .b = f(rgb.b)
  };
}

// factor is 0-100
QColor setLabBrightness(const QColor& in, qreal factor)
{
    const auto srgb = RGB{ in.redF(), in.greenF(), in.blueF() };
    const auto linearSRGB = SRGBToLinearSRGB(srgb);
    auto oklab = linearSRGBToOKLab(linearSRGB);
    oklab.L = factor;
    const auto linearSRGBModified = OKLabToLinearSRGB(oklab);
    const auto srgbModified = linearSRGBToSRGB(linearSRGBModified);

    return QColor::fromRgbF(cl(srgbModified.r), cl(srgbModified.g), cl(srgbModified.b));
}

void PaletteModel::setColor(const QColor& color)
{
    qWarning() << color << d->color;
    if (color == d->color)
        return;

    d->color = color;
    Q_EMIT colorChanged();

    for (int i = 1; i <= 20; i++)
    {
        d->colors[i-1] = setLabBrightness(color, i / 20.0);
    }

    Q_EMIT dataChanged(index(0), index(19));
}

int PaletteModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)

    return 20;
}

QVariant PaletteModel::data(const QModelIndex& idx, int role) const
{
    Q_UNUSED(role);

    return d->colors[idx.row()];
}


QHash<int, QByteArray> PaletteModel::roleNames() const
{
    return {{0, "mColor"}};
}


QVariant PaletteModel::tryParse(const QString& it)
{
    const auto c = CSSColorParser::parse(it.toStdString());
    if (!c.has_value()) {
        return QVariant();
    }
    return QColor::fromRgb(c->r, c->g, c->b);
}