// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <QAbstractListModel>
#include <QColor>

class PaletteModel : public QAbstractListModel
{

    Q_OBJECT

    struct Private;
    QScopedPointer<Private> d;

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

public:
    explicit PaletteModel(QObject* parent = nullptr);
    ~PaletteModel();

    QColor color() const;
    void setColor(const QColor& color);
    Q_SIGNAL void colorChanged();

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE QVariant tryParse(const QString& str);

};
