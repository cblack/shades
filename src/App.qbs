// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

QtApplication {
    name: "org.kde.shades"

    install: true
    installDir: "bin"

    files: ["*.cpp", "*.h"]

    cpp.cxxLanguageVersion: "c++17"

    Group {
        files: ["resources/**"]
        fileTags: "qt.core.resource_data"
        Qt.core.resourceSourceBase: "resources/"
        Qt.core.resourcePrefix: "/"
    }

    Depends { name: "Qt"; submodules: ["quick", "widgets"] }
}
