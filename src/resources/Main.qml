// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Layouts 1.10
import QtQuick.Controls 2.10 as QQC2
import QtQuick.Templates 2.10 as T
import org.kde.kirigami 2.12 as Kirigami
import org.kde.shades 1.0
import QtGraphicalEffects 1.12

Window {
    color: Kirigami.Theme.backgroundColor
    visible: true

    width: cont.implicitWidth
    minimumWidth: cont.implicitWidth
    height: cont.implicitHeight
    minimumHeight: cont.implicitHeight

    component Swatch : T.Control {
        id: cont

        required property color color

        implicitHeight: Math.max(implicitBackgroundHeight + topInset + bottomInset,
                                 implicitContentHeight + topPadding + bottomPadding)
        implicitWidth: Math.max(implicitBackgroundWidth + leftInset + rightInset,
                                implicitContentWidth + leftPadding + rightPadding)

        padding: 16
        Layout.fillWidth: true

        background: Rectangle {
            color: cont.color
        }
    }

    ColumnLayout {
        id: cont
        anchors.left: parent.left
        anchors.right: parent.right
        spacing: 0

        QQC2.ToolBar {
            Layout.fillWidth: true
            contentItem: RowLayout {
                QQC2.TextField {
                    id: colorField
                    onTextChanged: {
                        const color = palModel.tryParse(text)
                        if (color) {
                            palModel.color = color
                        }
                    }
                    Layout.fillWidth: true
                }
            }
        }
        Repeater {
            model: PaletteModel {
                id: palModel
                color: "#ff0000"
            }
            delegate: Swatch {
                id: swatch

                required property color mColor
                color: mColor
                readonly property int brightness: Kirigami.ColorUtils.brightnessForColor(mColor)

                QQC2.Label {
                    text: swatch.mColor.toString().toUpperCase()
                    color: swatch.brightness == Kirigami.ColorUtils.Light ?
                        "black" :
                        "white"

                    anchors.centerIn: parent

                    layer.enabled: true
                    layer.effect: DropShadow {
                        radius: 0.0
                        verticalOffset: 1
                        color: swatch.brightness == Kirigami.ColorUtils.Light ?
                            Qt.rgba(255, 255, 255, 0.4) :
                            Qt.rgba(0, 0, 0, 0.4)
                    }
                }
            }
        }
    }
}
