// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

Product {
    Group {
        files: ["org.kde.Shades.svg"]
        qbs.install: true
        qbs.installDir: "share/icons/hicolor/scalable/apps"
    }
    Group {
        files: ["org.kde.Shades.desktop"]
        qbs.install: true
        qbs.installDir: "share/applications"
    }
}